#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <dirent.h>
#define RULES_FILE "/etc/sdev/sdev.rule"
int sdev(const char* filename);
int interract(char*file,mode_t perm,uid_t owner,gid_t group);

int main(int argc,char* argv[]){
    return sdev(RULES_FILE);
}
int is_dir(const char* dname){
	struct stat statbuf;
	if(stat(dname,&statbuf)!=0){
	    return 0;
	}
	return S_ISDIR(statbuf.st_mode);
}
int is_exists(const char *fname)
{
    FILE *file;
    if ((file = fopen(fname, "r")))
    {
        fclose(file);
        return 1;
    }
    return 0;
}
int sdev(const char* filename){
    FILE *rule;
    rule=fopen(filename,"r");
    if(rule == NULL){
        printf("Error opening file : %s\n",filename);
        exit(1);
    }
    char *file=malloc(255*sizeof(char));
    char line[255];
    while (fscanf(rule,"%[^\n]\n",&line)!= EOF){
        mode_t perm=644;
        uid_t owner=-1;
        gid_t group=-1;
		sscanf(line,"%s %04o %o %o",file,&perm,&owner,&group);
		if(line[0] != '#'){
			if(strstr(line,"import")!=NULL){
		        char inc[255];
		        sscanf(line,"import %s",&inc);
		        sdev(inc);
	        }else{
				if(file[strlen(file)-1] == '*' && file[strlen(file)-2] == '/'){
				    DIR *dp;
                    struct dirent *dirp;
                    struct stat sb;
				    file[strlen(file)-1]=NULL;
				    if((dp = opendir(file))==NULL){
                        perror("can't open dir");
                    }
                    while((dirp = readdir(dp))!=NULL){
						char *fullfile=malloc(1024);
						strcpy(fullfile,file);
                        strcat(fullfile,dirp->d_name);
                        if(is_dir(fullfile)==0){
                            interract(fullfile,perm,owner,group);
					    }
                   }
				}else if(is_exists(file)){
					interract(file,perm,owner,group);
				}else{
				    printf("%s not exsist\n",file);	
				}
		    }
	    }
    }
    return 0;
}
int interract(char*file,mode_t perm,uid_t owner,gid_t group){
	
    if(chmod(file,perm)!=0){
    	fprintf(stderr,"Chmod error :%s\n",file);
    	return 1;
	}
	if(owner == -1 || group == -1){
		fprintf(stderr,"\033[32m%s \033[33m%04o\033[;0m\n",file,perm);
	    return 0;	
	}
    if(chown(file,owner,group)!=0){
	    fprintf(stderr,"Chown error :%s\n",file);
	    return 1;
	}
	fprintf(stderr,"\033[32m%s \033[33m%04o %o %o\033[;0m\n",file,perm,owner,group);
	return 0;
}
