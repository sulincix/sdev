build:
	$(CC) --static -o sdev sdev.c
clean:
	rm -f sdev
install: install-binary
	mkdir -p $(DESTDIR)/etc/sdev || true
	mkdir -p $(DESTDIR)/etc/init.d || true
	install sdev.rule $(DESTDIR)/etc/sdev/sdev.rule
	install sdev.service $(DESTDIR)/etc/init.d/sdev
install-binary:
	mkdir -p $(DESTDIR)/bin || true
	install sdev $(DESTDIR)/bin/sdev
